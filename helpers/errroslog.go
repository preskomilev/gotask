package helpers

import (
	"gotask/models"
	"sync"
)

type ErrorLogData struct {
	sync.Mutex
	data []interface{}
}

func (eld *ErrorLogData) Set(val interface{}) {
	eld.Lock()
	defer eld.Unlock()
	if len(eld.data) == 0 {
		eld.data = append(eld.data, val)
	} else {
		var addNew bool = true
		for index, a := range eld.data {
			sItem := (a).(models.ErrorLog)
			cItem := (val).(models.ErrorLog)
			if sItem.Expression == cItem.Expression &&
				sItem.Endpoint == cItem.Endpoint &&
				sItem.Type == cItem.Type {
				item := eld.data[index]
				subItem := (item).(models.ErrorLog)
				subItem.Frequency += 1
				eld.data[index] = subItem
				addNew = false
			}
		}
		if addNew {
			eld.data = append(eld.data, val)
		}
	}
}

func (eld *ErrorLogData) Get() []interface{} {
	eld.Lock()
	defer eld.Unlock()
	return eld.data
}
