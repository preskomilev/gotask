package controllers

import (
	"fmt"
	"gotask/models"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	errdata "gotask/helpers"

	"github.com/gin-gonic/gin"
)

var m errdata.ErrorLogData

const regex = "What is ([0-9]+) ([a-z]+)(by)?.* ([0-9]+)(\\?)?(([a-z]+)(by)? ([0-9]+))?.\\*?$"
const regexOperation = "What is ([0-9]+) (([a-z]+))*?.\\*?$"
const regexOnlyNumbers = "[-]?\\d[\\d,]*[\\.]?[\\d{2}]*"
const regexSyntax = ".*\\w.*\\s.*\\w.*"

func Evaluate(c *gin.Context) {
	var item models.ExpItem
	var res models.Result
	var operationOrder []string
	var numberResult = 0
	operationsWithSymbol := map[string]string{
		"plus":       "+",
		"minus":      "-",
		"multiplied": "*",
		"divided":    "/",
	}

	err := c.BindJSON(&item)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	check, reason := IsValid("/evaluate", item.Expression)

	if check {

		nMatch := regexp.MustCompile(regexOnlyNumbers)
		submatchall := nMatch.FindAllString(item.Expression, -1)

		nums := GetNumsArr(submatchall)
		operationsStr := GetOperArr(nums, item.Expression)

		for _, oper := range operationsStr {
			for k, v := range operationsWithSymbol {
				if strings.Contains(oper, k) {
					operationOrder = append(operationOrder, v)
				}
			}
		}

		for i := 0; i < len(nums); i++ {
			numberResult = nums[i]
			for _, oper := range operationOrder {
				if oper == "+" {
					numberResult = numberResult + nums[i+1]
					i++
				} else if oper == "-" {
					numberResult = numberResult - nums[i+1]
					i++
				} else if oper == "*" {
					numberResult = numberResult * nums[i+1]
					i++
				} else if oper == "/" {
					numberResult = numberResult / nums[i+1]
					i++
				}
			}
		}

		fmt.Println("result:", numberResult)
		res.Result = strconv.Itoa(numberResult)
		c.IndentedJSON(http.StatusOK, res)

	} else {
		resp := make(map[string]string)
		resp["error"] = reason
		c.IndentedJSON(http.StatusOK, resp)
	}
}

func Validate(c *gin.Context) {
	var item models.ExpItem

	err := c.BindJSON(&item)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	check, reason := IsValid("/validate", item.Expression)
	if check {
		resp := make(map[string]interface{})
		resp["valid"] = check
		c.IndentedJSON(http.StatusOK, resp)
	} else {
		resp := make(map[string]interface{})
		resp["valid"] = check
		resp["reason"] = reason
		c.IndentedJSON(http.StatusOK, resp)
	}
}

func Errors(c *gin.Context) {
	items := m.Get()
	c.IndentedJSON(http.StatusOK, items)
}

func IsValid(endpoin string, expression string) (bool, string) {

	var checkOperation bool
	var checkExpression bool
	operators := []string{"plus", "minus", "multiplied", "divided"}
	var operatorsInOrder []string

	var item models.ErrorLog
	item.Expression = expression
	item.Endpoint = endpoin
	item.Frequency = 1

	matchUnsuppOper, _ := regexp.MatchString(regexOperation, expression)
	if matchUnsuppOper {
		textArr := strings.Split(expression, ` `)
		for _, word := range textArr {
			for _, val := range operators {
				if word == val {
					checkOperation = true
				}
			}
		}
		if !checkOperation {
			item.Type = "Unsupported operations"
			m.Set(item)
			return false, "Unsupported operations"
		}
	}

	checkExpression, _ = regexp.MatchString(regex, expression)
	if checkExpression {

		nMatch := regexp.MustCompile(regexOnlyNumbers)
		submatchall := nMatch.FindAllString(expression, -1)

		nums := GetNumsArr(submatchall)
		operationsStr := GetOperArr(nums, expression)

		for i := range operationsStr {
			checkSyntax, _ := regexp.MatchString(regexSyntax, operationsStr[i])
			if checkSyntax {
				item.Type = "Expressions with invalid syntax"

				m.Set(item)
				return false, "Expressions with invalid syntax"
			}
		}

		if len(nums) == len(operationsStr) {
			item.Type = "Expressions with invalid syntax"

			m.Set(item)
			return false, "Expressions with invalid syntax"
		}

		if len(nums)-1 >= len(operationsStr)+1 {
			item.Type = "Expressions with invalid syntax"

			m.Set(item)
			return false, "Expressions with invalid syntax"
		}

		checkSyntax, _ := regexp.MatchString(regexSyntax, operationsStr[0])
		if checkSyntax {
			item.Type = "Expressions with invalid syntax"

			m.Set(item)
			return false, "Expressions with invalid syntax"
		}

		for _, oper := range operationsStr {
			for _, val := range operators {
				if strings.Contains(oper, val) {
					operatorsInOrder = append(operatorsInOrder, val)
				}
			}
		}

		if len(operatorsInOrder) < 1 || len(operatorsInOrder) != len(operationsStr) {
			item.Type = "Unsupported operations"
			m.Set(item)
			return false, "Unsupported operations"
		}

	} else {
		item.Type = "Non-math questions"
		m.Set(item)
		return false, "Non-math questions"
	}

	return true, ""
}

func GetNumsArr(submatchall []string) []int {
	var nums []int
	for _, element := range submatchall {
		number, _ := strconv.Atoi(element)
		nums = append(nums, number)
	}
	return nums
}

func GetOperArr(nums []int, expression string) []string {
	var stringSumMatch []string
	rpPattern := `What is \d{1,5} ([^\.]+)(by)?.* \d{1,5} (\?)?(?:([^\.]+)(by)? \d{1,5})?.\\*?`
	rpTest := regexp.MustCompile(rpPattern)
	expression = strings.ReplaceAll(expression, "by", "")
	fMatch, _ := regexp.MatchString(rpPattern, expression)

	if fMatch {
		stringSumMatch = rpTest.FindStringSubmatch(expression)
		stringSumMatch = append(stringSumMatch[:0], stringSumMatch[0+1:]...)
	} else {
		rpPattern = `What is \d{1,5} ([^\.]+)(by)? \d{1,5}.\\*?`
		rpTest = regexp.MustCompile(rpPattern)
		stringSumMatch = rpTest.FindStringSubmatch(expression)
		stringSumMatch = append(stringSumMatch[:0], stringSumMatch[0+1:]...)
	}

	var r []string
	for _, str := range stringSumMatch {
		if str != "" {
			str = strings.TrimSpace(str)
			r = append(r, str)
		}
	}

	return r
}
