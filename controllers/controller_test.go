package controllers

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/require"
)

func TestEvaluate(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	w := httptest.NewRecorder()
	body := bytes.NewBuffer([]byte("{\"expression\":\"What is 5 plus 2?\"}"))
	req, err := http.NewRequest("POST", "/evaluate", body)
	require.Nil(t, err)
	req.Header.Set("Content-Type", "application-json")
	r.ServeHTTP(w, req)
}

func TestValidate(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	w := httptest.NewRecorder()
	body := bytes.NewBuffer([]byte("{\"expression\":\"What is 5 plus 2?\"}"))
	req, err := http.NewRequest("POST", "/validate", body)
	require.Nil(t, err)
	req.Header.Set("Content-Type", "application-json")
	r.ServeHTTP(w, req)
}

func TestErrors(t *testing.T) {
	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	Errors(c)
	if w.Code == 200 {
		t.Logf("Success")
	}
}

func TestGetNumsArr(t *testing.T) {
	var nums = []string{"5", "2"}
	var expected = []int{5, 2}
	observed := GetNumsArr(nums)

	if !reflect.DeepEqual(observed, expected) {
		t.Errorf("Expected %d. Got %d.", expected, observed)
	}
}

func TestGetOperArr(t *testing.T) {
	var nums = []int{5, 2}
	var expression string = "What is 5 plus 2?"
	var expected = []string{"plus"}

	observed := GetOperArr(nums, expression)

	if !reflect.DeepEqual(observed, expected) {
		t.Errorf("Expected %s. Got %s.", expected, observed)
	}
}

func TestIsValid(t *testing.T) {

	type expectedModel struct {
		check  bool
		reason string
	}

	var endpoin string = "/evaluate"
	var expression string = "What is 2 plus 2?"
	expected := expectedModel{true, ""}

	check, _ := IsValid(endpoin, expression)

	if check != expected.check {
		t.Errorf("Expected check %t. Got %t",
			expected.check, check)
	}

}
