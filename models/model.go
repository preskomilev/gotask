package models

type ExpItem struct {
	Expression string `json:"expression"`
}

type Result struct {
	Result string `json:"result"`
}

type Validate struct {
	Valid  bool   `json:"valid"`
	Reason string `json:"reason"`
}

type ErrorLog struct {
	Expression string `json:"expression"`
	Endpoint   string `json:"endpoint"`
	Frequency  int    `json:"frequency"`
	Type       string `json:"type"`
}
