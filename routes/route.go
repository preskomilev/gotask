package routes

import (
	controller "gotask/controllers"
	"os"

	"github.com/gin-gonic/gin"
)

type Routes struct {
}

func (c Routes) StartGin(port string) {
	r := gin.Default()

	r.Use(func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	})

	api := r.Group(os.Getenv("API_URL"))
	{
		api.POST("evaluate", controller.Evaluate)
		api.POST("validate", controller.Validate)
		api.GET("errors", controller.Errors)
	}
	r.Run(":" + port)
}
